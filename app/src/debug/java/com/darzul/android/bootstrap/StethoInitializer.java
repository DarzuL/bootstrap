package com.darzul.android.bootstrap;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import okhttp3.OkHttpClient;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 11/10/16.
 */

public final class StethoInitializer {

    private StethoInitializer() {
    }

    public static void init(App app) {
        Stetho.initializeWithDefaults(app);
    }

    public static void init(OkHttpClient.Builder okHttpClient) {
        okHttpClient.addNetworkInterceptor(new StethoInterceptor());
    }
}
