package com.darzul.android.bootstrap.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import com.compagny.android.chronos.R;
import com.darzul.android.bootstrap.bootstrap.activity.BaseActivity;
import com.darzul.android.bootstrap.fragment.MainFragment;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 2/20/16.
 *
 * Main activity
 */

public class MainActivity extends BaseActivity {

    /**
     * Tag
     */
    private static final String TAG = "MainActivity";
    private static final String MAIN_FRAGMENT_TAG = "MainFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar("Toolbar");

        FragmentManager fm = getSupportFragmentManager();
        if (fm.findFragmentByTag(MAIN_FRAGMENT_TAG) == null) {
            fm.beginTransaction(). //
                    add(R.id.container, MainFragment.newInstance(), MAIN_FRAGMENT_TAG). //
                    commit();
        }
    }
}
