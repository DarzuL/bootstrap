package com.darzul.android.bootstrap.bootstrap.helper;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 7/24/16.
 */

public class TextWatcherImplemented implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
