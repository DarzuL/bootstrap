package com.darzul.android.bootstrap.bootstrap.fragment.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import com.compagny.android.chronos.R;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 15/02/16.
 * <p>
 * Base dialog
 */

public abstract class BaseDialog extends DialogFragment {

    public static final int BUTTON_STYLE_ORANGE = 0;

    protected Listener mListener;

    private int mLayoutRes = R.layout.dialog_with_horizontal_buttons;
    private int mButtonStyle = BUTTON_STYLE_ORANGE;
    private int mButtonLayoutRes = R.layout.dialog_button_horizontal;
    private int mButtonTextRes = -1;
    private int mButtonBackgroundRes = -1;

    public BaseDialog setVerticalButton(boolean verticalButton) {
        if (verticalButton) {
            mLayoutRes = R.layout.dialog_with_vertical_buttons;
            mButtonLayoutRes = R.layout.dialog_button_vertical;
            return this;
        }

        mLayoutRes = R.layout.dialog_with_horizontal_buttons;
        mButtonLayoutRes = R.layout.dialog_button_horizontal;
        return this;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        return inflater.inflate(mLayoutRes, container, false);
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupButtons(view);
    }

    /**
     * Fix from http://stackoverflow.com/questions/14657490/how-to-properly-retain-a-dialogfragment-through-rotation
     */
    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    public BaseDialog setListener(Listener listener) {
        mListener = listener;
        return this;
    }

    public BaseDialog setButtonStyle(@IntRange(from = 0, to = 4) int buttonStyle) {
        mButtonStyle = buttonStyle;
        return this;
    }

    private void setupButtons(View rootView) {
        LinearLayout buttonsLayout = (LinearLayout) rootView.findViewById(R.id.ll_buttons);
        if (buttonsLayout == null) {
            return;
        }

        int[] buttonsTextRes = getButtonsTextRes();
        if (buttonsTextRes.length <= 0) {
            return;
        }

        applyButtonStyle();

        LayoutInflater inflater = getActivity().getLayoutInflater();
        for (final int buttonTextRes : buttonsTextRes) {
            Button button = (Button) inflater.inflate(
                    mButtonLayoutRes,
                    buttonsLayout,
                    false
            );
            button.setText(buttonTextRes);
            button.setTextColor(ContextCompat.getColorStateList(getContext(), mButtonTextRes));
            button.setBackgroundResource(mButtonBackgroundRes);

            button.setOnClickListener(v -> {
                if (mListener != null) {
                    mListener.onClick(buttonTextRes);
                }

                dismiss();
            });

            buttonsLayout.addView(button);
        }
    }

    private void applyButtonStyle() {
        switch (mButtonStyle) {
            case BUTTON_STYLE_ORANGE:
                mButtonTextRes = R.drawable.text_button_dialog_orange;
                mButtonBackgroundRes = R.drawable.bg_button_dialog_orange;
                break;

            default:
                throw (new IllegalArgumentException("Style: " + mButtonStyle + " is unknown"));
        }
    }

    protected abstract int[] getButtonsTextRes();

    public interface Listener {
        void onClick(@StringRes int buttonTextRes);
    }
}