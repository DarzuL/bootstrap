package com.darzul.android.bootstrap.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.compagny.android.chronos.R;
import com.darzul.android.bootstrap.bootstrap.fragment.dialog.InfoDialog;
import com.darzul.bootstrap.fragment.BaseFragment;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 2/20/16.
 * <p>
 * Main fragment
 */

public class MainFragment extends BaseFragment {

    public static MainFragment newInstance() {
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_main;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main_fragment, menu);
    }

    @Override
    protected void bindViews(View rootView) {
        rootView.findViewById(R.id.btn_primary)
                .setOnClickListener(v -> InfoDialog.newInstance(
                        R.string.dialog_title,
                        R.string.dialog_content,
                        R.string.dialog_action_cancel,
                        R.string.dialog_action_validate
                )
                        .show(getFragmentManager(), null));
    }
}
