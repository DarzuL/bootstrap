package com.darzul.android.bootstrap.bootstrap.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 2/20/16.
 *
 * Simple class to decorate recycler view items
 */

public class SimpleItemDecoration extends RecyclerView.ItemDecoration {
    private final int mSpaceRes;
    private final int mColorRes;
    private final int mItemPerLine;

    private Paint mPaint;
    private int mSpace;
    private int mSemiSpace;

    public SimpleItemDecoration(Context context, @DimenRes int spaceRes, @ColorRes int colorRes, int itemPerLine) {
        mSpaceRes = spaceRes;
        mColorRes = colorRes;
        mItemPerLine = itemPerLine;

        mSpace = context.getResources().getDimensionPixelSize(mSpaceRes);
        mSemiSpace = mSpace / 2;

        mPaint = new Paint();
        mPaint.setColor(ContextCompat.getColor(context, mColorRes));
        mPaint.setStrokeWidth(mSpace);
    }

    public SimpleItemDecoration(Context context, @DimenRes int dimenRes, @ColorRes int colorRes) {
        this(context, dimenRes, colorRes, 1);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = parent.getChildAt(i);
            int position = parent.getChildAdapterPosition(view);

            c.save();

            if (!isFirstRow(position)) {
                c.translate(0, view.getTop() - mSemiSpace);
                c.drawLine(0, 0, parent.getWidth(), 0, mPaint);
            }

            if (!isFirstCol(position)) {
                c.translate(0, view.getLeft() - mSemiSpace);
                c.drawLine(0, 0, 0, parent.getHeight(), mPaint);
            }

            c.restore();
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int left = 0, top = 0, right = 0, bottom = 0;

        int position = parent.getChildAdapterPosition(view);
        if (!isFirstRow(position)) {
            top = mSpace;
        }

        if (!isFirstCol(position)) {
            left = mSpace;
        }

        outRect.set(left, top, right, bottom);
    }

    /**
     * @param position item position obtained by RecyclerView.getChildAdapterPosition(View)
     * @return True if the position correspond to one in the first row
     */
    private boolean isFirstRow(int position) {
        return position < mItemPerLine;
    }

    /**
     * @param position item position obtained by RecyclerView.getChildAdapterPosition(View)
     * @return True if the position correspond to one in the first col
     */
    private boolean isFirstCol(int position) {
        return mItemPerLine == 1 || position % mItemPerLine == 0;
    }
}
