package com.darzul.android.bootstrap.fragment;

import com.compagny.android.chronos.BuildConfig;
import com.crashlytics.android.Crashlytics;
import com.darzul.bootstrap.presenter.DefaultQueryErrorManager;
import com.darzul.bootstrap.presenter.QueryStateManager;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 2/3/17.
 */

public class ExampleQueryErrorManager extends DefaultQueryErrorManager {

    public ExampleQueryErrorManager(QueryStateManager.View view) {
        super(view);
    }

    @Override
    protected void onUnhandledHttpCodeError(int code) {
        RuntimeException e = new NullPointerException("Code's handler " + code + " isn't defined");
        if (BuildConfig.DEBUG) {
            throw e;
        }

        Crashlytics.logException(e);
    }
}
