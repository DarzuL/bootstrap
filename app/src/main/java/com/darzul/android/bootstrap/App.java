package com.darzul.android.bootstrap;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 2/20/16.
 *
 * Chronos App
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());
        StethoInitializer.init(this);
    }
}
