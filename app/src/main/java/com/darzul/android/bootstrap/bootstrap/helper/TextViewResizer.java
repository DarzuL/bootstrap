package com.darzul.android.bootstrap.bootstrap.helper;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by DarzuL on 23/07/2016.
 */

public class TextViewResizer {

    public boolean mIsMeasuring;

    /**
     * Compute {@link TextView} width for a given text then ensure the {@link TextView} has always the same width even
     * if the text changed. You shouldn't change the text until the {@link Listener onMeasured} has been called
     *
     * @param textView {@link TextView} with a blocked width
     * @param text Text used to measure the {@link TextView} width
     * @param listener Listener to know when the TextView has been measured
     */
    public void computeMaxWidth(final TextView textView, String text, final Listener listener) {
        mIsMeasuring = true;
        textView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop,
                                       int oldRight, int oldBottom) {
                v.removeOnLayoutChangeListener(this);
                attachSizeWatcher(textView, right - left);
                mIsMeasuring = false;

                listener.onMeasured();
            }
        });
        textView.setText(text);
    }

    private void attachSizeWatcher(final TextView textView, final int width) {
        textView.addTextChangedListener(new TextWatcherImplemented() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ViewGroup.LayoutParams lp = textView.getLayoutParams();
                lp.width = width;
                textView.setLayoutParams(lp);
            }
        });
    }

    public boolean isMeasuring() {
        return mIsMeasuring;
    }

    public interface Listener {
        void onMeasured();
    }
}
