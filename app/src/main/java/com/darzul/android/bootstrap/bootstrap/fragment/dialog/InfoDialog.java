package com.darzul.android.bootstrap.bootstrap.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.TextView;

import com.compagny.android.chronos.R;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 2/20/16.
 * <p>
 * Simple dialog with title + text
 */

public class InfoDialog extends BaseDialog {

    private static final String ARG_TITLE = "arg:title";
    private static final String ARG_MESSAGE = "arg:message";
    private static final String ARG_BUTTONS_TEXT = "arg:buttons_text";

    public static InfoDialog newInstance(
            @StringRes int titleRes,
            @StringRes int messageRes,
            @StringRes int... buttonsTextRes
    ) {
        Bundle args = new Bundle();
        args.putInt(ARG_TITLE, titleRes);
        args.putInt(ARG_MESSAGE, messageRes);
        args.putIntArray(ARG_BUTTONS_TEXT, buttonsTextRes);
        InfoDialog infoDialog = new InfoDialog();
        infoDialog.setArguments(args);

        return infoDialog;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView titleTv = (TextView) view.findViewById(R.id.tv_title);
        TextView messageTv = (TextView) view.findViewById(R.id.tv_message);

        Bundle args = getArguments();
        titleTv.setText(args.getInt(ARG_TITLE));
        messageTv.setText(args.getInt(ARG_MESSAGE));
    }

    @Override
    protected int[] getButtonsTextRes() {
        return getArguments().getIntArray(ARG_BUTTONS_TEXT);
    }
}
