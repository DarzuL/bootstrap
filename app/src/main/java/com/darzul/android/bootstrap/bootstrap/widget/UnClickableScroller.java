package com.darzul.android.bootstrap.bootstrap.widget;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 27/11/15.
 *
 * From: https://github.com/AndroidDeveloperLB/LollipopContactsRecyclerViewFastScroller
 */

public class UnClickableScroller extends LinearLayout {

    private View handle;
    private RecyclerView recyclerView;
    private int height;
    private final RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(final RecyclerView recyclerView, final int dx, final int dy) {
            computeHandlerPosition();
        }
    };

    private boolean isInitialized = false;

    public UnClickableScroller(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public UnClickableScroller(final Context context) {
        super(context);
        init();
    }

    public UnClickableScroller(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    protected void init() {
        if (isInitialized) {
            return;
        }
        isInitialized = true;
        setOrientation(HORIZONTAL);
        setClipChildren(false);
    }

    public void setViewsToUse(@LayoutRes int layoutResId, @IdRes int handleResId) {
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(layoutResId, this, true);
        handle = findViewById(handleResId);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        height = h;
        computeHandlerPosition();
    }

    public void setRecyclerView(final RecyclerView recyclerView) {
        if (this.recyclerView != recyclerView) {
            if (this.recyclerView != null) {
                this.recyclerView.removeOnScrollListener(onScrollListener);
            }
            this.recyclerView = recyclerView;
            if (this.recyclerView == null) {
                return;
            }
            recyclerView.addOnScrollListener(onScrollListener);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (recyclerView != null) {
            recyclerView.removeOnScrollListener(onScrollListener);
            recyclerView = null;
        }
    }

    private int getValueInRange(int min, int max, int value) {
        int minimum = Math.max(min, value);
        return Math.min(minimum, max);
    }

    private void computeHandlerPosition() {
        final int verticalScrollOffset = recyclerView.computeVerticalScrollOffset();
        final int verticalScrollRange = recyclerView.computeVerticalScrollRange();
        float proportion = (float) verticalScrollOffset / ((float) verticalScrollRange - height);
        setHandlerPosition(height * proportion);
    }

    private void setHandlerPosition(float y) {
        final int handleHeight = handle.getHeight();
        handle.setY(getValueInRange(0, height - handleHeight, (int) (y - handleHeight / 2)));
    }
}
