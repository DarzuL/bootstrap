package com.darzul.android.bootstrap;

import okhttp3.OkHttpClient;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 11/10/16.
 */

public final class StethoInitializer {

    private StethoInitializer() {
    }

    public static void init(App app) {
        // nothing
    }

    public static void init(OkHttpClient.Builder okHttpClient) {
        // nothing
    }
}

