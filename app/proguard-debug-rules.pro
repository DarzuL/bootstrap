-include proguard-rules.pro

# Debug
-dontobfuscate
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*,!code/allocation/variable

-keep class com.darzul.** {*; }