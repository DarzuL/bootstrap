package com.darzul.bootstrap.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 21/04/16.
 * <p>
 * Basic method to represent Fragment Lifecycle
 */

public interface FragmentLifeCycle {

    void onCreate(Bundle savedInstanceState);

    void onViewCreated();

    void onSaveInstanceState(Bundle outState);

    void onViewStateRestored(Activity activity, @Nullable Bundle savedInstanceState);

    void onDestroyView();

    void onDestroy();
}
