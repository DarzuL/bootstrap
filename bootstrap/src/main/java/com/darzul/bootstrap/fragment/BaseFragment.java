package com.darzul.bootstrap.fragment;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.darzul.bootstrap.presenter.ViewCreatedSubject;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 2/20/16.
 * <p>
 * Base fragment extended by all fragments in app
 */

public abstract class BaseFragment extends Fragment {

    private ViewCreatedSubject mViewCreatedSubject;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewCreatedSubject = new ViewCreatedSubject.Builder().build();
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(getLayout(), container, false);
    }

    @LayoutRes
    protected abstract int getLayout();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
        mViewCreatedSubject.onNext(true);
    }

    @Override
    public void onDestroyView() {
        mViewCreatedSubject.onNext(false);
        super.onDestroyView();
    }

    public ViewCreatedSubject getViewCreatedSubject() {
        return mViewCreatedSubject;
    }

    protected abstract void bindViews(View rootView);
}
