package com.darzul.bootstrap.form.validator;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 5/14/16.
 * <p>
 * Form need a validator to validate the user inputs
 */

public interface FormValidator<D> {
    boolean isValid(D data);
}
