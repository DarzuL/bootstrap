package com.darzul.bootstrap.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.darzul.bootstrap.adapter.viewholder.ViewHolder;

import java.util.List;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 11/05/16.
 * <p>
 * Adapter able to manage a loading state. When the adapter enter in loading mode,
 * it displays the specified view as a Footer. It's useful in infinite list,
 * if the user reaches the end of the list before data are fetched,
 * he'll see a loader so he'll know more data are incoming.
 */

public abstract class LoadingAdapter<I> extends HeaderAndFooterAdapter<I> {

    protected static final int VIEW_TYPE_LOADING = 997;
    protected static final int VIEW_TYPE_NETWORK_ERROR = 996;

    private boolean mLoading = false;
    private boolean mNetworkError = false;
    private boolean mLoadedAllData = false;

    public LoadingAdapter(Context context, List<I> data) {
        super(context, data);
    }

    public void setLoading(boolean loading) {
        int itemCount = getItemCount();
        mLoading = loading;

        if (loading) {
            notifyItemInserted(itemCount);
            return;
        }

        notifyItemRemoved(itemCount);
    }

    public void setNetworkError(boolean networkError) {
        int itemCount = getItemCount();
        mNetworkError = networkError;

        if (networkError) {
            notifyItemInserted(itemCount);
            return;
        }

        notifyItemRemoved(itemCount);
    }

    public void setLoadedAllData(boolean loadedAllData) {
        mLoadedAllData = loadedAllData;
    }

    public abstract void setItems(List<I> items);

    public abstract void addItems(List<I> items);

    public abstract View getLoadingViewItem(ViewGroup parent);

    public abstract View getNetworkErrorViewItem(ViewGroup parent);

    @Override
    public int getItemViewType(int position) {
        if (mLoadedAllData) {
            return super.getItemViewType(position);
        }

        if (position == getItemCount() - 1) {
            if (mLoading) {
                return VIEW_TYPE_LOADING;
            }

            if (mNetworkError) {
                return VIEW_TYPE_NETWORK_ERROR;
            }
        }

        // Footer shouldn't be displayed until all data fetch
        if (isFooter(position)) {
            return VIEW_TYPE_NULL;
        }

        return super.getItemViewType(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_LOADING:
                return new LoadingViewHolder(getLoadingViewItem(parent));

            case VIEW_TYPE_NETWORK_ERROR:
                return new NetworkErrorViewHolder(getNetworkErrorViewItem(parent));

            default:
                return super.onCreateViewHolder(parent, viewType);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int viewType = getItemViewType(position);

        if (viewType == VIEW_TYPE_LOADING || viewType == VIEW_TYPE_NETWORK_ERROR) {
            return;
        }

        super.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        int count = mLoading || mNetworkError ? 1 : 0;
        return count + super.getItemCount();
    }

    private static class LoadingViewHolder extends ViewHolder<Void> {

        private LoadingViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void refreshView(Void data) {
            // nothing
        }
    }

    private static class NetworkErrorViewHolder extends ViewHolder<Void> {

        private NetworkErrorViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void refreshView(Void data) {
            // nothing
        }
    }
}