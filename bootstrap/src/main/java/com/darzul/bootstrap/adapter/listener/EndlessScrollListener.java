package com.darzul.bootstrap.adapter.listener;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Evaneos on 01/06/2015.
 * <p>
 * Listen the RecyclerView scroll an notify when the end of the list is almost reached
 */

public abstract class EndlessScrollListener extends RecyclerView.OnScrollListener {

    private static final int THRESHOLD = 5;

    // The total number of items in the data set after the last load
    private int mPreviousTotal = 0;
    // True if we are still waiting for the last set of data to load.
    private boolean mLoading = true;
    private boolean mBottomIsReached = false;

    private LinearLayoutManager mLinearLayoutManager;

    public EndlessScrollListener(LinearLayoutManager linearLayoutManager) {
        mLinearLayoutManager = linearLayoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (mBottomIsReached) {
            return;
        }

        int totalItemCount = mLinearLayoutManager.getItemCount();
        int lastItemPosition = totalItemCount - 1;
        int lastVisibleItemPosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();

        if (mLoading) {
            if (totalItemCount > mPreviousTotal + 1) {
                mLoading = false;
                mPreviousTotal = totalItemCount;
            }
        }

        if (!mLoading && lastVisibleItemPosition >= lastItemPosition - THRESHOLD) {
            mLoading = true;
            onBottomReached();
        }
    }

    public void setBottomReached(boolean bottomIsReached) {
        mBottomIsReached = bottomIsReached;
    }

    public abstract void onBottomReached();
}
