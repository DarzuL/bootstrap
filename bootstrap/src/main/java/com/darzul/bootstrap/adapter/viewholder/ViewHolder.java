package com.darzul.bootstrap.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 5/14/16.
 */
public abstract class ViewHolder<I> extends RecyclerView.ViewHolder {
    public ViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void refreshView(I item);
}
