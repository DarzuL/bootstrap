package com.darzul.bootstrap.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.darzul.bootstrap.R;
import com.darzul.bootstrap.adapter.viewholder.NullViewHolder;
import com.darzul.bootstrap.adapter.viewholder.ViewHolder;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 19/08/16.
 * <p>
 * Adapter able to maanger headers and footer as View
 */

public abstract class HeaderAndFooterAdapter<I> extends AdapterWithContext<ViewHolder> {

    protected static final int VIEW_TYPE_HEADER = 999;
    protected static final int VIEW_TYPE_FOOTER = 998;

    protected final List<I> mData;
    private final List<View> mHeaders = new LinkedList<>();
    private final List<View> mFooters = new LinkedList<>();

    public HeaderAndFooterAdapter(Context context, List<I> data) {
        super(context);
        mData = data;
    }

    public void addHeader(View header) {
        mHeaders.add(header);
        notifyItemInserted(mHeaders.size() - 1);
    }

    public void removeHeader(View header) {
        if (!mHeaders.contains(header)) {
            return;
        }

        int index = mHeaders.indexOf(header);
        mHeaders.remove(index);
        notifyItemRemoved(index);
    }

    public void addFooter(View footer) {
        mFooters.add(footer);
        notifyItemInserted(mHeaders.size() + mData.size() + mFooters.size() - 1);
    }

    public void removeFooter(View footer) {
        if (!mFooters.contains(footer)) {
            return;
        }

        int index = mFooters.indexOf(footer);
        mFooters.remove(index);
        notifyItemRemoved(mHeaders.size() + mData.size() + index);
    }

    protected boolean isHeader(int position) {
        return position >= 0 && position < mHeaders.size();
    }

    private int getFooterPosition(int position) {
        return position - (getItemCount() - mFooters.size());
    }

    protected boolean isFooter(int position) {
        int footerPosition = getFooterPosition(position);
        return footerPosition >= 0 && footerPosition < mFooters.size();
    }

    @Override
    public int getItemCount() {
        return mHeaders.size() + mData.size() + mFooters.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isHeader(position)) {
            return VIEW_TYPE_HEADER;
        }

        if (isFooter(position)) {
            return VIEW_TYPE_FOOTER;
        }

        return getItemViewTypeWithAdjustedPosition(position - mHeaders.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);

        if (viewType == VIEW_TYPE_HEADER || viewType == VIEW_TYPE_FOOTER) {
            return new HeaderOrFooterViewHolder(inflater.inflate(
                    R.layout.view_holder_header_footer,
                    parent,
                    false
            ));
        }

        return new NullViewHolder(inflater.inflate(R.layout.view_holder_null, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int viewType = getItemViewType(position);

        switch (viewType) {
            case VIEW_TYPE_HEADER:
                holder.refreshView(mHeaders.get(position));
                return;

            case VIEW_TYPE_FOOTER:
                holder.refreshView(mFooters.get(getFooterPosition(position)));
                return;

            case VIEW_TYPE_NULL:
                return;

            default:
                onBindViewHolderWithAdjustedPosition(holder, position - mHeaders.size());
        }
    }

    protected abstract int getItemViewTypeWithAdjustedPosition(int position);

    protected abstract void onBindViewHolderWithAdjustedPosition(ViewHolder holder, int position);

    private static class HeaderOrFooterViewHolder extends ViewHolder<View> {

        private final ViewGroup mLayout;

        public HeaderOrFooterViewHolder(View itemView) {
            super(itemView);
            mLayout = (ViewGroup) itemView;
        }

        @Override
        public void refreshView(View item) {
            mLayout.removeAllViews();

            if (item.getParent() != null) {
                ((ViewGroup) item.getParent()).removeAllViews();
            }

            mLayout.addView(item);
        }
    }
}
