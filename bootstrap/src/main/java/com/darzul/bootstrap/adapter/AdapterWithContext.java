package com.darzul.bootstrap.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.darzul.bootstrap.adapter.viewholder.ViewHolder;

/**
 * Created by DarzuL on 29/07/2015.
 * <p>
 * Adapter with context. All adapters need a Context to inflate ViewHolder view
 */

public abstract class AdapterWithContext<VH extends ViewHolder> extends RecyclerView.Adapter<VH> {

    protected static final int VIEW_TYPE_NULL = -1;
    protected static final int VIEW_TYPE_UNDEFINED = 0;

    public Context mContext;

    public AdapterWithContext(Context context) {
        mContext = context;
    }

    public void setContext(Context context) {
        mContext = context;
    }
}
