package com.darzul.bootstrap.adapter.viewholder;

import android.view.View;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 19/08/16.
 */
public class NullViewHolder extends ViewHolder<Void> {

    public NullViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void refreshView(Void item) {
        // nothing
    }
}
