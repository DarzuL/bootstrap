package com.darzul.bootstrap.data;

import java.util.List;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 03/11/16.
 * <p>
 * Generic interface used to not depends on data source when you need to use pagination
 */

public interface PaginatedData<T> {
    int SOURCE_DB = 1;
    int SOURCE_API = 2;

    List<T> getData();

    boolean hasNext();

    String getNextLink();

    int getSource();
}

