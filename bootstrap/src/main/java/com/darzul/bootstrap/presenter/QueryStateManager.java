package com.darzul.bootstrap.presenter;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 12/23/16.
 * <p>
 * Interface to implement to manage a query state (waiting|success|error)
 */

public interface QueryStateManager {

    void onQueryStarted();

    void onQuerySucceed();

    void onQueryError(Throwable t);

    void destroy();

    interface View {
        void hideQueryError();

        void displayNetworkError();

        void displayUnknownError();
    }
}
