package com.darzul.bootstrap.presenter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.darzul.bootstrap.adapter.LoadingAdapter;
import com.darzul.bootstrap.adapter.listener.EndlessScrollListener;
import com.darzul.bootstrap.data.PaginatedData;

import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 11/05/16.
 * <p>
 * Presenter with an endless list
 */

public abstract class EndlessScrollPresenter<T extends Parcelable> extends SingleQueryPresenter<PaginatedData<T>>
        implements EndlessScrollContract.Presenter {

    private LoadingAdapter mLoadingAdapter;
    private EndlessScrollListener mEndlessScrollListener;

    private EndlessScrollContract.View mView;
    private PaginatedData<T> mLastDataFetched;

    private Subscription mDataSubscription;

    protected EndlessScrollPresenter(
            Context context,
            EndlessScrollContract.View view,
            LoadingAdapter loadingAdapter,
            QueryStateManager queryStateManager
    ) {
        super(view, queryStateManager);
        mView = view;
        mLoadingAdapter = loadingAdapter;
        mLoadingAdapter.setContext(context);
    }

    @Override
    public void onViewCreated() {
        RecyclerView recyclerView = mView.getRecyclerView();
        attachEndlessScrollListener(recyclerView);
    }

    private void attachEndlessScrollListener(RecyclerView recyclerView) {
        LinearLayoutManager layoutManager;
        try {
            layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        } catch (ClassCastException e) {
            throw new RuntimeException(
                    "EndlessScrollPresenter manage only RecyclerView with a LinearLayoutManager");
        }

        mEndlessScrollListener = new EndlessScrollListener(layoutManager) {
            @Override
            public void onBottomReached() {
                createNextQuery();
            }
        };
        recyclerView.addOnScrollListener(mEndlessScrollListener);
    }

    @Override
    protected void onDataFetched(PaginatedData<T> data) {
        mLastDataFetched = data;
    }

    private void createNextQuery() {
        if (mLastDataFetched == null || !mLastDataFetched.hasNext()) {
            mEndlessScrollListener.setBottomReached(true);
            mLoadingAdapter.setLoadedAllData(true);
            return;
        }

        mLoadingAdapter.setLoading(true);
        mDataSubscription = prepareNextQuery(mLastDataFetched).subscribeOn(Schedulers.io())
                .lift(new PausableOperator<>(mViewCreatedSubject.getObservable(), true))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                    mLastDataFetched = r;
                    mData.getData()
                            .addAll(r.getData());
                    onNextDataFetched(r.getData());
                    mLoadingAdapter.setLoading(false);
                }, t -> {
                    mLoadingAdapter.setLoading(false);
                    mLoadingAdapter.setNetworkError(true);
                });
    }

    protected abstract Observable<PaginatedData<T>> prepareNextQuery(PaginatedData<T> lastDataFetched);

    protected abstract void onNextDataFetched(List<T> data);

    @Override
    public void reloadLastPage() {
        mLoadingAdapter.setNetworkError(false);
        createNextQuery();
    }

    @Override
    public void onViewStateRestored(Activity activity, @Nullable Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            return;
        }

        super.onViewStateRestored(activity, savedInstanceState);
        mLoadingAdapter.setContext(activity);
    }

    @Override
    public void onDestroy() {
        if (mDataSubscription != null) {
            mDataSubscription.unsubscribe();
        }

        mView = null;
        super.onDestroy();
    }
}