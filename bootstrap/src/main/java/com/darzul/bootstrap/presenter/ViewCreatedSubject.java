package com.darzul.bootstrap.presenter;

import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 08/11/16.
 * <p>
 * Subject wrapper with an event stream of view created/destroyed
 */

public class ViewCreatedSubject {

    private final Subject<Boolean, Boolean> mSubject;

    private ViewCreatedSubject() {
        mSubject = BehaviorSubject.create();
    }

    public void onNext(boolean viewCreated) {
        mSubject.onNext(viewCreated);
    }

    public Observable<Boolean> getObservable() {
        return mSubject;
    }

    public void destroy() {
        mSubject.onCompleted();
    }

    public static class Builder {
        public ViewCreatedSubject build() {
            return new ViewCreatedSubject();
        }
    }
}
