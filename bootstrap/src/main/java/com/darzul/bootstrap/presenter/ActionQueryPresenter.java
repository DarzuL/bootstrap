package com.darzul.bootstrap.presenter;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.annimon.stream.Stream;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by DarzuL on 23/07/2016.
 * <p>
 * Presenter to use when you have a view with several actions
 */

public class ActionQueryPresenter implements ActionQueryContract.Presenter {

    private final List<PresenterAction> mActions = new LinkedList<>();

    protected ViewCreatedSubject mViewCreatedSubject;
    protected LoadingSubject mLoadingSubject;
    private QueryStateManager mQueryStateManager;

    public ActionQueryPresenter(
            ActionQueryContract.View view,
            QueryStateManager queryStateManager
    ) {
        mViewCreatedSubject = view.getViewCreatedSubject();

        mLoadingSubject = new LoadingSubject.Builder().build(mViewCreatedSubject, view);
        mLoadingSubject.onNext(false);

        mQueryStateManager = queryStateManager;
    }

    @Override
    public int addAction(Action action) {
        mActions.add(toPresenterAction(action));
        return mActions.size() - 1;
    }

    @Override
    public void performAction(int actionId) {
        performAction(actionId, true, true);
    }

    @Override
    public void performAction(int actionId, boolean displayLoading, boolean displayNetworkError) {
        PresenterAction action = mActions.get(actionId);
        action.perform(displayLoading, displayNetworkError);
    }

    @Override
    public void performAction(Action action) {
        performAction(action, true, true);
    }

    @Override
    public void performAction(Action action, boolean displayLoading, boolean displayNetworkError) {
        toPresenterAction(action)
                .perform(displayLoading, displayNetworkError);
    }

    private PresenterAction toPresenterAction(Action action) {
        return new PresenterAction(
                action,
                mViewCreatedSubject,
                mLoadingSubject,
                mQueryStateManager
        );
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // nothing
    }

    @Override
    public void onViewCreated() {
        // nothing
    }

    @Override
    public void onDestroyView() {
        // nothing
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // nothing
    }

    @Override
    public void onViewStateRestored(Activity activity, @NonNull Bundle savedInstanceState) {
        // nothing
    }

    @Override
    public void onDestroy() {
        mViewCreatedSubject.destroy();
        mLoadingSubject.destroy();
        mQueryStateManager.destroy();

        Stream.of(mActions).forEach(PresenterAction::destroy);
    }
}

