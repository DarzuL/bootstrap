package com.darzul.bootstrap.presenter;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 12/29/16.
 * <p>
 * Interface to implement to manage NetworkQuery error
 */

public interface QueryErrorManager {
    void onHttpError(int code);

    void onNetworkError(Throwable throwable);

    void onUnknownError(Throwable throwable);
}
