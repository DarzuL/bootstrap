package com.darzul.bootstrap.presenter;

import com.darzul.bootstrap.fragment.FragmentLifeCycle;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 11/18/16.
 * <p>
 * SingleQuery contract
 */

public interface SingleQueryContract {

    interface View extends LoadingSubject.View, QueryStateManager.View, EmptyDataSubject.View {
        ViewCreatedSubject getViewCreatedSubject();
    }

    interface Presenter extends FragmentLifeCycle {
        void reload();
    }
}
