package com.darzul.bootstrap.presenter;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 9/9/16.
 * <p>
 * EndlessScroll contract
 */

public interface EndlessScrollContract {

    interface View extends SingleQueryContract.View {
        RecyclerView getRecyclerView();
    }

    interface Presenter extends SingleQueryContract.Presenter {
        void reloadLastPage();
    }
}
