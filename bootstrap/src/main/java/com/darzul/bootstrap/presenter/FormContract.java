package com.darzul.bootstrap.presenter;


import com.darzul.bootstrap.fragment.FragmentLifeCycle;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 11/18/16.
 * <p>
 * Form contract
 */

public interface FormContract {

    interface View<D> extends LoadingSubject.View, QueryStateManager.View {
        /**
         * @param formData you have to update the formData values with the values from the UI
         */
        void fillFormData(D formData);

        /**
         * @param formData you have to complete the UI with the date from formData
         */
        void fillForm(D formData);

        ViewCreatedSubject getViewCreatedSubject();
    }

    interface Presenter extends FragmentLifeCycle {
    }
}
