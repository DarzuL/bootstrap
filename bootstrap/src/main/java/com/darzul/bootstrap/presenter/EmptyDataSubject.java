package com.darzul.bootstrap.presenter;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 15/11/16.
 * <p>
 * Subject wrapper with an event stream of empty data state
 */

public class EmptyDataSubject {
    private final Subject<Boolean, Boolean> mSubject;
    private Subscription mSubscription;
    private EmptyDataSubject.View mView;

    private EmptyDataSubject(ViewCreatedSubject viewCreatedSubject, EmptyDataSubject.View view) {
        mSubject = BehaviorSubject.create();
        Observable<Boolean> viewCreatedObservable = viewCreatedSubject.getObservable();
        mSubscription = mSubject.subscribeOn(Schedulers.io())
                .lift(new PausableOperator<>(viewCreatedObservable, true))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(state -> {
                    if (state) {
                        mView.displayEmptyData();
                    } else {
                        mView.hideEmptyData();
                    }
                });

        mView = view;
    }

    public void onNext(boolean isEmpty) {
        mSubject.onNext(isEmpty);
    }

    public Observable<Boolean> getObservable() {
        return mSubject;
    }

    public void destroy() {
        mSubject.onCompleted();
        mSubscription.unsubscribe();
        mView = null;
    }

    public interface View {
        void displayEmptyData();

        void hideEmptyData();
    }

    public static class Builder {
        public EmptyDataSubject build(
                ViewCreatedSubject viewCreatedSubject,
                EmptyDataSubject.View view
        ) {
            return new EmptyDataSubject(viewCreatedSubject, view);
        }
    }
}
