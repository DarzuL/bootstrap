package com.darzul.bootstrap.presenter;

import android.util.SparseArray;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 12/29/16.
 * <p>
 * Basic {@link QueryErrorManager} impl.
 * Manage query error, specially http one where you can attach a specific handler foreach code.
 */

public abstract class DefaultQueryErrorManager implements QueryErrorManager {

    private final SparseArray<CodeErrorHandler> mCodeErrorHandlers;
    private QueryStateManager.View mView;

    public DefaultQueryErrorManager(QueryStateManager.View view) {
        mView = view;
        mCodeErrorHandlers = new SparseArray<>();
    }

    @Override
    public void onHttpError(int code) {
        if (mCodeErrorHandlers.get(code) == null) {
            onUnhandledHttpCodeError(code);
            mView.displayUnknownError();
            return;
        }

        mCodeErrorHandlers.get(code).handle();
    }

    @Override
    public void onNetworkError(Throwable throwable) {
        mView.displayNetworkError();
    }

    @Override
    public void onUnknownError(Throwable throwable) {
        mView.displayUnknownError();
    }

    /**
     * Call when there's any handler for the specific http code error.
     * Generally you want to log it for release app and crash for debug one.
     *
     * @param code http code error
     */
    protected abstract void onUnhandledHttpCodeError(int code);

    public void addCodeErrorHandler(int code, CodeErrorHandler handler) {
        mCodeErrorHandlers.put(code, handler);
    }

    public interface CodeErrorHandler {
        void handle();
    }
}
