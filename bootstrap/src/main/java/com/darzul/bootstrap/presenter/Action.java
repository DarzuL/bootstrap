package com.darzul.bootstrap.presenter;

import rx.Observable;

/**
 * Created by DarzuL on 23/07/2016.
 * <p>
 * An action is used by ActionQueryPresenter to exec a query
 */

public abstract class Action<R> {

    public abstract Observable<R> query();

    public abstract void success(R result);

    public abstract void error(Throwable t);
}
