package com.darzul.bootstrap.presenter;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 08/11/16.
 * <p>
 * Operator to resume/pause a stream
 */

public class PausableOperator<T> implements Observable.Operator<T, T> {

    private final Observable<Boolean> mPausable;
    private final boolean mReverseLogic;

    private boolean mStarted;

    /**
     * @param pausable True to pause the stream and false to resume it
     */
    public PausableOperator(Observable<Boolean> pausable) {
        this(pausable, false);
    }

    /**
     * @param pausable     True to pause the stream and false to resume it
     * @param reverseLogic Reverse the pausable boolean logic, so if reverseLogic is set to true: true will resume the
     *                     stream and false pause it. Default is false.
     */
    public PausableOperator(Observable<Boolean> pausable, boolean reverseLogic) {
        mPausable = pausable;
        mReverseLogic = reverseLogic;
    }

    @Override
    public Subscriber<? super T> call(Subscriber<? super T> s) {
        return new Subscriber<T>(s) {
            @Override
            public void onCompleted() {
                if (!mStarted) {
                    s.onCompleted();
                }
            }

            @Override
            public void onError(Throwable t) {
                if (!s.isUnsubscribed()) {
                    s.onError(t);
                }
            }

            @Override
            public void onNext(T data) {
                if (s.isUnsubscribed()) {
                    return;
                }

                mStarted = true;
                Observable.just(data)
                        .flatMap(v -> mPausable.distinctUntilChanged()
                                .map(isPaused -> mReverseLogic ? !isPaused : isPaused)
                                .filter(isPaused -> !isPaused)
                                .map(viewCreated -> v))
                        .subscribe(s::onNext);
            }
        };
    }
}
