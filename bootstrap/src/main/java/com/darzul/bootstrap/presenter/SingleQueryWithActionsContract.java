package com.darzul.bootstrap.presenter;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 12/23/16.
 * <p>
 * SingleQuery contract
 */

public interface SingleQueryWithActionsContract {

    interface View extends SingleQueryContract.View, ActionQueryContract.View {

    }

    interface Presenter extends ActionQueryContract.Presenter {
    }
}
