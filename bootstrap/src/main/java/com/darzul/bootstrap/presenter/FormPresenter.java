package com.darzul.bootstrap.presenter;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.darzul.bootstrap.form.validator.FormValidator;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 11/18/16.
 * <p>
 * Presenter used when you have a form
 */

public abstract class FormPresenter<D extends Parcelable, R> implements FormContract.Presenter {

    private static final String STATE_FORM_DATA = "state:form_data";

    private final ViewCreatedSubject mViewCreatedSubject;
    private final LoadingSubject mLoadingSubject;
    private final FormValidator<D> mFormValidator;
    private final QueryStateManager mQueryStateManager;

    protected D mFormData;

    private FormContract.View<D> mView;
    private Subscription mSubscription;

    public FormPresenter(
            FormContract.View<D> view,
            FormValidator<D> formValidator,
            D formData,
            QueryStateManager queryStateManager
    ) {
        mView = view;

        mViewCreatedSubject = view.getViewCreatedSubject();

        mLoadingSubject = new LoadingSubject.Builder().build(mViewCreatedSubject, view);
        mLoadingSubject.onNext(false);

        mFormValidator = formValidator;
        mFormData = formData;

        mQueryStateManager = queryStateManager;
    }

    /**
     * Must be called to send form data
     */
    protected void prepareQuery() {
        updateFormData();
        if (!mFormValidator.isValid(mFormData)) {
            return;
        }

        mLoadingSubject.onNext(true);

        unsubscribe();
        mSubscription = query(mFormData).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    mLoadingSubject.onNext(false);
                    callSuccess(result);
                }, t -> {
                    mLoadingSubject.onNext(false);
                    mQueryStateManager.onQueryError(t);
                });
    }

    private void callSuccess(R result) {
        onSuccess(result);
    }

    protected abstract Observable<R> query(D formData);

    protected abstract void onSuccess(R result);

    @Override
    public void onViewStateRestored(Activity activity, @Nullable Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            return;
        }

        mFormData = savedInstanceState.getParcelable(STATE_FORM_DATA);
        mView.fillForm(mFormData);
    }

    @Override
    public void onViewCreated() {
        try {
            mView.fillForm(mFormData);
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    "FormPresenter<D, R> and FormContract.View<D> must have the same D type");
        }
    }

    @Override
    public void onDestroyView() {
        // nothing
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        updateFormData();
        outState.putParcelable(STATE_FORM_DATA, mFormData);
    }

    @Override
    public void onDestroy() {
        mView = null;

        mViewCreatedSubject.destroy();
        mLoadingSubject.destroy();
        mQueryStateManager.destroy();

        unsubscribe();
    }

    private void updateFormData() {
        try {
            mView.fillFormData(mFormData);
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    "FormPresenter<D, R> and FormContract.View<D> must have the same D type");
        }
    }

    private void unsubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }
}
