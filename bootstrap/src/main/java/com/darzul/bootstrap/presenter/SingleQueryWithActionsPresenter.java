package com.darzul.bootstrap.presenter;

/**
 * Created by DarzuL on 23/07/2016.
 * <p>
 * SingleQueryPresenter which accept Actions
 */

public abstract class SingleQueryWithActionsPresenter<R> extends SingleQueryPresenter<R>
        implements SingleQueryWithActionsContract.Presenter {

    private ActionQueryPresenter mActionQueryPresenter;

    public SingleQueryWithActionsPresenter(
            SingleQueryWithActionsContract.View view,
            QueryStateManager queryStateManager
    ) {
        super(view, queryStateManager);
        mActionQueryPresenter = new ActionQueryPresenter(view, queryStateManager);
    }

    @Override
    public int addAction(Action action) {
        return mActionQueryPresenter.addAction(action);
    }

    @Override
    public void performAction(int actionId, boolean displayLoading, boolean displayNetworkError) {
        mActionQueryPresenter.performAction(actionId, displayLoading, displayNetworkError);
    }

    @Override
    public void performAction(int actionId) {
        mActionQueryPresenter.performAction(actionId);
    }

    @Override
    public void performAction(Action action) {
        mActionQueryPresenter.performAction(action);
    }

    @Override
    public void performAction(Action action, boolean displayLoading, boolean displayNetworkError) {
        mActionQueryPresenter.performAction(action, displayLoading, displayNetworkError);
    }

    @Override
    public void onDestroy() {
        mActionQueryPresenter.onDestroy();
        super.onDestroy();
    }
}
