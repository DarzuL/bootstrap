package com.darzul.bootstrap.presenter;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 08/11/16.
 * <p>
 * Subject wrapper with an event stream of loading state
 */

public class LoadingSubject {

    private final Subject<Boolean, Boolean> mSubject;
    private Subscription mSubscription;
    private View mView;

    private LoadingSubject(ViewCreatedSubject viewCreatedSubject, View view) {
        mSubject = BehaviorSubject.create();
        Observable<Boolean> viewCreatedObservable = viewCreatedSubject.getObservable();
        mSubscription = mSubject.subscribeOn(Schedulers.io())
                .lift(new PausableOperator<>(viewCreatedObservable, true))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(state -> {
                    if (state) {
                        mView.displayLoading();
                    } else {
                        mView.hideLoading();
                    }
                });

        mView = view;
    }

    public void onNext(boolean isLoading) {
        mSubject.onNext(isLoading);
    }

    public Observable<Boolean> getObservable() {
        return mSubject;
    }

    public void destroy() {
        mSubject.onCompleted();
        mSubscription.unsubscribe();
        mView = null;
    }

    public interface View {
        void displayLoading();

        void hideLoading();
    }

    public static class Builder {
        public LoadingSubject build(ViewCreatedSubject viewCreatedSubject, View view) {
            return new LoadingSubject(viewCreatedSubject, view);
        }
    }
}
