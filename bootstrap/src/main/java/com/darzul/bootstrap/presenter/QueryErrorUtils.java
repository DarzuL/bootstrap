package com.darzul.bootstrap.presenter;

import java.io.IOException;

import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 12/23/16.
 * <p>
 * Check retrofit2 Throwable to specify the cause (no network|http error|unknown)
 */

public final class QueryErrorUtils {

    private QueryErrorUtils() {
    }

    public static void onError(Throwable throwable, QueryErrorManager queryErrorManager) {
        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            queryErrorManager.onHttpError(httpException.code());
            return;
        }

        if (throwable instanceof IOException) {
            queryErrorManager.onNetworkError(throwable);
            return;
        }

        queryErrorManager.onUnknownError(throwable);
    }
}
