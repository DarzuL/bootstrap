package com.darzul.bootstrap.presenter;


import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by DarzuL on 23/07/2016.
 * <p>
 * Wrap an Action to add View|Loading|Query state management
 */

public class PresenterAction<R> {

    private final Action<R> mAction;
    private ViewCreatedSubject mViewCreatedSubject;
    private LoadingSubject mLoadingSubject;
    private QueryStateManager mQueryStateManager;

    private Subscription mActionSubscription;

    public PresenterAction(
            Action<R> action,
            ViewCreatedSubject viewCreatedSubject,
            LoadingSubject loadingPresenter,
            QueryStateManager queryStateManager
    ) {
        mAction = action;
        mViewCreatedSubject = viewCreatedSubject;
        mLoadingSubject = loadingPresenter;
        mQueryStateManager = queryStateManager;
    }

    public void perform(boolean displayLoading, boolean displayQueryError) {
        if (displayLoading) {
            mLoadingSubject.onNext(true);
        }

        mActionSubscription = mAction.query()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(r -> {
                    if (displayLoading) {
                        mLoadingSubject.onNext(false);
                    }

                    if (displayQueryError) {
                        mQueryStateManager.onQuerySucceed();
                    }
                })
                .lift(new PausableOperator<>(mViewCreatedSubject.getObservable(), true))
                .subscribe(mAction::success, t -> {
                    if (displayLoading) {
                        mLoadingSubject.onNext(false);
                    }

                    if (displayQueryError) {
                        mQueryStateManager.onQueryError(t);
                    }

                    mAction.error(t);
                });
    }

    public void destroy() {
        mActionSubscription.unsubscribe();

        mViewCreatedSubject = null;
        mLoadingSubject = null;
        mQueryStateManager = null;
    }
}