package com.darzul.bootstrap.presenter;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 12/23/16.
 * <p>
 * Basic impl of QueryStateManager
 */

public class DefaultQueryStateManager implements QueryStateManager {

    private View mView;
    private QueryErrorManager mQueryErrorManager;
    private Subject<Message, Message> mSubject;
    private Subscription mSubscription;

    public DefaultQueryStateManager(
            ViewCreatedSubject viewCreatedSubject,
            View view,
            QueryErrorManager queryErrorManager
    ) {
        mView = view;

        mQueryErrorManager = queryErrorManager;

        mSubject = BehaviorSubject.create(new Message(Message.TYPE_DEFAULT));
        mSubscription = mSubject.subscribeOn(Schedulers.io())
                .lift(new PausableOperator<>(viewCreatedSubject.getObservable(), true))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(m -> {
                    switch (m.type) {
                        case Message.TYPE_DEFAULT:
                        case Message.TYPE_STARTED:
                        case Message.TYPE_SUCCEED:
                            mView.hideQueryError();
                            return;

                        case Message.TYPE_ERROR:
                            QueryErrorUtils.onError(m.throwable, mQueryErrorManager);
                    }
                });
    }

    @Override
    public void onQueryStarted() {
        mSubject.onNext(new Message(Message.TYPE_STARTED));
    }

    @Override
    public void onQuerySucceed() {
        mSubject.onNext(new Message(Message.TYPE_SUCCEED));
    }

    @Override
    public void onQueryError(Throwable t) {
        mSubject.onNext(new Message(Message.TYPE_ERROR, t));
    }

    @Override
    public void destroy() {
        mSubject.onCompleted();
        mSubscription.unsubscribe();
        mView = null;
    }

    private static class Message {
        static final int TYPE_DEFAULT = 0;
        static final int TYPE_STARTED = 1;
        static final int TYPE_SUCCEED = 2;
        static final int TYPE_ERROR = 3;

        public final int type;
        public final Throwable throwable;

        public Message(int type) {
            this(type, null);
        }

        public Message(int type, Throwable throwable) {
            this.type = type;
            this.throwable = throwable;
        }
    }
}
