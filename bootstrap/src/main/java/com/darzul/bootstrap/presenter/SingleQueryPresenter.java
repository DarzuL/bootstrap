package com.darzul.bootstrap.presenter;

import android.app.Activity;
import android.os.Bundle;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 4/28/16.
 * <p>
 * Presenter with only one query to fetch its data. It'll manage several state: loading, error, empty, viewCreated
 */

public abstract class SingleQueryPresenter<R> implements SingleQueryContract.Presenter {

    protected R mData;
    protected ViewCreatedSubject mViewCreatedSubject;
    protected LoadingSubject mLoadingSubject;
    private EmptyDataSubject mEmptyDataSubject;
    private QueryStateManager mQueryStateManager;

    private Subscription mDataSubscription;

    protected SingleQueryPresenter(
            SingleQueryContract.View view,
            QueryStateManager queryStateManager
    ) {
        mViewCreatedSubject = view.getViewCreatedSubject();

        mLoadingSubject = new LoadingSubject.Builder().build(mViewCreatedSubject, view);
        mLoadingSubject.onNext(false);

        mEmptyDataSubject = new EmptyDataSubject.Builder().build(mViewCreatedSubject, view);
        mEmptyDataSubject.onNext(false);

        mQueryStateManager = queryStateManager;
    }

    /**
     * Need to be call to perform the query. Generally called in constructor
     */
    protected void prepareQuery() {
        mLoadingSubject.onNext(true);
        mQueryStateManager.onQueryStarted();

        mDataSubscription = query().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(r -> {
                    mLoadingSubject.onNext(false);
                    mEmptyDataSubject.onNext(isEmpty(r));
                })
                .filter(r -> !isEmpty(r))
                .doOnNext(data -> {
                    mData = data;
                    onDataFetched(data);
                })
                .lift(new PausableOperator<>(mViewCreatedSubject.getObservable(), true))
                .subscribe(this::onDataFetchedAndViewCreated, t -> {
                    mLoadingSubject.onNext(false);
                    mEmptyDataSubject.onNext(false);
                    mQueryStateManager.onQueryError(t);
                });
    }

    protected abstract boolean isEmpty(R data);

    protected abstract Observable<R> query();

    protected abstract void onDataFetched(R data);

    protected abstract void onDataFetchedAndViewCreated(R data);

    /**
     * Perform a new query to fetch the data. Generally called by the reload button
     */
    @Override
    public void reload() {
        mQueryStateManager.onQueryStarted();
        prepareQuery();
    }

    @Override
    public void onViewCreated() {
        // nothing
    }

    @Override
    public void onDestroyView() {
        // nothing
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // nothing
    }

    @Override
    public void onViewStateRestored(Activity activity, Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            return;
        }

        if (mData != null && !isEmpty(mData)) {
            onDataFetchedAndViewCreated(mData);
        }
    }

    @Override
    public void onDestroy() {
        mViewCreatedSubject.destroy();
        mLoadingSubject.destroy();
        mEmptyDataSubject.destroy();

        if (mDataSubscription != null) {
            mDataSubscription.unsubscribe();
        }
    }
}
