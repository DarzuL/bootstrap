package com.darzul.bootstrap.presenter;


import com.darzul.bootstrap.fragment.FragmentLifeCycle;

/**
 * Created by Guillaume 'DarzuL' Bourderye on 12/23/16.
 * <p>
 * ActionQuery contract
 */

public interface ActionQueryContract {

    interface View extends LoadingSubject.View, QueryStateManager.View {
        ViewCreatedSubject getViewCreatedSubject();
    }

    interface Presenter extends FragmentLifeCycle {
        int addAction(Action action);

        void performAction(int actionId, boolean displayLoading, boolean displayNetworkError);

        void performAction(int actionId);

        void performAction(Action action);

        void performAction(Action action, boolean displayLoading, boolean displayNetworkError);
    }
}
