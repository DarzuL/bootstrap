#!/bin/bash

PROJECT_PATH=/home/darzul/Git/marvel

SCREENSHOT_DIRECTORY_NAME="screenshots"
DEVICE_SCREENSHOT_DIRECTORY_PATH="/data/local/tmp/$SCREENSHOT_DIRECTORY_NAME" 
LOCAL_SCREENSHOT_DIRECTORY_PATH="/tmp/$SCREENSHOT_DIRECTORY_NAME"

adb pull $DEVICE_SCREENSHOT_DIRECTORY_PATH $LOCAL_SCREENSHOT_DIRECTORY_PATH

cd $LOCAL_SCREENSHOT_DIRECTORY_PATH
for picturePath in $(find -name "*.png")
do
  echo $picturePath
  # Crop the bottom 144px
  # because Nexus 5 has a 144px height navigation bar
  convert -quality 100 $picturePath -gravity South -chop 0x144 $picturePath
done
