#!/bin/bash

if [ $# -lt 2 ]; then
    echo "Usage: $0 input output"
    exit 1
fi

BASE_DPI=90

MDPI=$(echo "$BASE_DPI * 1" | bc -l)
HDPI=$(echo "$BASE_DPI * 1.5" | bc -l)
XHDPI=$(echo "$BASE_DPI * 2" | bc -l)
XXHDPI=$(echo "$BASE_DPI * 3" | bc -l)
XXXHDPI=$(echo "$BASE_DPI * 4" | bc -l)

#mkdir -p $2/res/drawable-mdpi
mkdir -p $2/res/drawable-hdpi
mkdir -p $2/res/drawable-xhdpi
mkdir -p $2/res/drawable-xxhdpi
#mkdir -p $2/res/drawable-xxxhdpi

if [[ -d $1 ]]; then
  for path in $(find $1/ -name '*.svg')
  do
    filename=$(basename ${path%.*})
    echo $filename
    #inkscape --export-png "$2/res/drawable-mdpi/$filename.png" -d $MDPI $path
    inkscape --export-png "$2/res/drawable-hdpi/$filename.png" -d $HDPI $path
    inkscape --export-png "$2/res/drawable-xhdpi/$filename.png" -d $XHDPI $path
    inkscape --export-png "$2/res/drawable-xxhdpi/$filename.png" -d $XXHDPI $path
    #inkscape --export-png "$2/res/drawable-xxxhdpi/$filename.png" -d $XXXHDPI $path
  done
elif [[ -f $1 ]]; then
  filename=$(basename ${1%.*})
  inkscape --export-png "$2/res/drawable-hdpi/$filename.png" -d $HDPI $1
  inkscape --export-png "$2/res/drawable-xhdpi/$filename.png" -d $XHDPI $1
  inkscape --export-png "$2/res/drawable-xxhdpi/$filename.png" -d $XXHDPI $1
else
  echo "$1 is not valid"
  exit 1
fi
