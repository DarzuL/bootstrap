#!/bin/bash

##
# START CONFIG
##
OUTPUT="app/src/main/res"
STYLE_APP_NAME="TODO"
##
# END CONFIG
##

function validType() {
    case ${1} in
    'primary')
        ;;
    'secondary')
        ;;
    'dialog')
        ;;
    *)
        echo "Don't know type: ${1}"
        exit 1
        ;;
    esac
}

function validColorName() {
    if [[ $1 == *"_"* ]] ; then
        echo "Color name has to be in CamelCase"
        exit 1
    fi
}

function validParameters() {
    if [ $# -lt 2 ]; then
        echo "Usage: $0 (primary|secondary|dialog) colorName"
        exit 1
    fi

    validType $1
    validColorName $2
}

function toSnakeCase() {
    echo $1 | sed -r 's/([A-Z])/_\L\1/g'
}

validParameters $1 $2

type="$1"
typeWithFirstLetterUp="$(tr '[:lower:]' '[:upper:]' <<< ${type:0:1})${type:1}"

colorName="$2"
color_name=$(toSnakeCase "$2")
colorNameWithFirstLetterUp="$(tr '[:lower:]' '[:upper:]' <<< ${color_name:0:1})${color_name:1}"

echo '<?xml version="1.0" encoding="utf-8"?>
<resources>
    <color name="'${type}'Button_'${colorName}'_border_disabled">TODO</color>
    <color name="'${type}'Button_'${colorName}'_border_normal">TODO</color>
    <color name="'${type}'Button_'${colorName}'_border_pressed">TODO</color>
    <color name="'${type}'Button_'${colorName}'_disabled">TODO</color>
    <color name="'${type}'Button_'${colorName}'_normal">TODO</color>
    <color name="'${type}'Button_'${colorName}'_pressed">TODO</color>
    <color name="'${type}'Button_'${colorName}'_ripple">TODO</color>
    <color name="'${type}'Button_'${colorName}'_text_disabled">TODO</color>
    <color name="'${type}'Button_'${colorName}'_text_normal">TODO</color>
    <color name="'${type}'Button_'${colorName}'_text_pressed">TODO</color>

    <style name="'$STYLE_APP_NAME'.Widget.'${typeWithFirstLetterUp}'Button.'${colorNameWithFirstLetterUp}'" parent="'$STYLE_APP_NAME'.Widget.Button">
        <item name="android:textColor">@drawable/text_button_'${type}'_'${color_name}'</item>
        <item name="android:background">@drawable/bg_button_'${type}'_'${color_name}'</item>
    </style>
</resources>' | tee $OUTPUT/values/style_button_${type}_${color_name}.xml

echo '<?xml version="1.0" encoding="utf-8"?>
<selector xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:drawable="@drawable/bg_button_'${type}'_'${color_name}'_disabled" android:state_enabled="false" />
    <item android:drawable="@drawable/bg_button_'${type}'_'${color_name}'_pressed" android:state_focused="true" />
    <item android:drawable="@drawable/bg_button_'${type}'_'${color_name}'_pressed" android:state_pressed="true" />
    <item android:drawable="@drawable/bg_button_'${type}'_'${color_name}'_normal" />
</selector>' | tee $OUTPUT/drawable/bg_button_${type}_${color_name}.xml

echo '<?xml version="1.0" encoding="utf-8"?>
<ripple xmlns:android="http://schemas.android.com/apk/res/android"
    android:color="@color/'${type}'Button_'${colorName}'_ripple">
    <item android:id="@android:id/mask">
        <shape>
            <corners android:radius="@dimen/'${type}'Button_corner_radius" />
            <solid android:color="@color/'${type}'Button_'${colorName}'_ripple" />
        </shape>
    </item>
    <item>
        <selector>
            <item
                android:drawable="@drawable/bg_button_'${type}'_'${color_name}'_disabled"
                android:state_enabled="false" />
            <item android:drawable="@drawable/bg_button_'${type}'_'${color_name}'_normal" />
        </selector>
    </item>
</ripple>' | tee $OUTPUT/drawable-v21/bg_button_${type}_${color_name}.xml

states=( "normal" "pressed" "disabled" )
for state in "${states[@]}" ; do
echo '<shape xmlns:android="http://schemas.android.com/apk/res/android"
    android:shape="rectangle">
    <corners android:radius="@dimen/'${type}'Button_corner_radius" />
    <solid android:color="@color/'${type}'Button_'${colorName}'_'${state}'" />
    <stroke
        android:color="@color/'${type}'Button_'${colorName}'_border_'${state}'"
        android:width="@dimen/'${type}'Button_border_width" />
</shape>' | tee $OUTPUT/drawable/bg_button_${type}_${color_name}_${state}.xml
done

echo '<?xml version="1.0" encoding="utf-8"?>
<selector xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:color="@color/'${type}'Button_'${colorName}'_text_disabled" android:state_enabled="false" />
    <item android:color="@color/'${type}'Button_'${colorName}'_text_pressed" android:state_focused="true" />
    <item android:color="@color/'${type}'Button_'${colorName}'_text_pressed" android:state_pressed="true" />
    <item android:color="@color/'${type}'Button_'${colorName}'_text_normal" />
</selector> ' | tee $OUTPUT/drawable/text_button_${type}_${color_name}.xml
