#!/bin/bash

if [ $# -lt 2 ]; then
    echo "Usage: $0 ic_launcher.svg output"
    exit 1
fi

BASE_DPI=90

MDPI=$(echo "$BASE_DPI * 1" | bc -l)
HDPI=$(echo "$BASE_DPI * 1.5" | bc -l)
XHDPI=$(echo "$BASE_DPI * 2" | bc -l)
XXHDPI=$(echo "$BASE_DPI * 3" | bc -l)
XXXHDPI=$(echo "$BASE_DPI * 4" | bc -l)

mkdir -p $2/res/mipmap-mdpi
mkdir -p $2/res/mipmap-hdpi
mkdir -p $2/res/mipmap-xhdpi
mkdir -p $2/res/mipmap-xxhdpi
mkdir -p $2/res/mipmap-xxxhdpi

inkscape --export-png "$2/res/mipmap-mdpi/ic_launcher.png" -d $MDPI "$1"
inkscape --export-png "$2/res/mipmap-hdpi/ic_launcher.png" -d $HDPI "$1"
inkscape --export-png "$2/res/mipmap-xhdpi/ic_launcher.png" -d $XHDPI "$1"
inkscape --export-png "$2/res/mipmap-xxhdpi/ic_launcher.png" -d $XXHDPI "$1"
inkscape --export-png "$2/res/mipmap-xxxhdpi/ic_launcher.png" -d $XXXHDPI "$1"
inkscape --export-png "$2/res/ic_launcher.png" -w 512 $PLAYSOTRE "$1"
